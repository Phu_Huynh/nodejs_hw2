const express = require('express');
const Note = require('../../models/note');

const tokenCheck = require('../../middlewares/tokenCheck');
const notes = new express.Router();

notes.use(tokenCheck);

// GET notes for a users with limit and offset
notes.get('/', async (req, res) => {
  try {
    const user = req.user;
    let {offset, limit} = req.query;
    offset = !!parseInt(offset) ? parseInt(offset) : 0;
    limit = !!parseInt(limit) ? parseInt(limit) : 0;

    await user.populate({
      path: 'notes',
      options: {
        skip: parseInt(offset),
        limit: parseInt(limit),
      },
    });
    res.status(200).json({
      offset: offset,
      limit: limit,
      count: user.notes.length,
      notes: user.notes,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

// Add Note for User, payload: {text: string}
notes.post('/', async (req, res) => {
  try {
    const {text} = req.body;
    const note = new Note({userId: req.user._id, text});
    await note.save();

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

// GET user's note by id
notes.get('/:id', async (req, res) => {
  try {
    const {id} = req.params;
    const note = await Note.findOne({_id: id});

    if (!note) {
      res.status(400).json({message: 'Invalid note id!'});
      return;
    }
    res.status(200).json(note);
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

// Update user's note by id
notes.put('/:id', async (req, res) => {
  try {
    const {id} = req.params;
    const {text} = req.body;
    const note = await Note.findOne({_id: id});

    if (!note) {
      res.status(400).json({message: 'Invalid note id!'});
      return;
    }
    note.text = text;
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

/* Check/uncheck user's note by id
value for completed field should be changed to opposite */
notes.patch('/:id', async (req, res) => {
  try {
    const {id} = req.params;
    const note = await Note.findOne({_id: id});

    if (!note) {
      res.status(400).json({message: 'Invalid note id!'});
      return;
    }
    note.completed = !note.completed;
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

// Delete user's note by id
notes.delete('/:id', async (req, res) => {
  try {
    const {id} = req.params;
    const ack = await Note.deleteOne({_id: id});

    if (ack.deletedCount !== 1) {
      res.status(400).json({message: 'Invalid note id!'});
      return;
    }
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = notes;
