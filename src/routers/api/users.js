const express = require('express');

const bcrypt = require('bcrypt');
const tokenCheck = require('../../middlewares/tokenCheck');

const User = require('../../models/user');
const users = new express.Router();

users.use(tokenCheck);

// GET requesting user profile
users.get('/me', async (req, res) => {
  res.send(req.user);
});

// CHANGE user password
users.patch('/me', async (req, res) => {
  try {
    const {oldPassword, newPassword} = req.body;
    const user = req.user;

    const isPasswordMatch = await bcrypt.compare(oldPassword, user.password);

    if (!isPasswordMatch) {
      res.status(400).json({message: 'oldPassword does not match'});
      return;
    }

    user.password = newPassword;
    await user.save();

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

// DELETE user's profile
users.delete('/me', async (req, res) => {
  try {
    const user = req.user;
    await User.deleteOne({_id: user._id});
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = users;
