const express = require('express');
const {Router} = express;

const users = require('./users');
const auth = require('./auth');
const notes = require('./notes');

const apiRouter = new Router();

apiRouter.use('/users', users);
apiRouter.use('/auth', auth);
apiRouter.use('/notes', notes);

module.exports = apiRouter;
