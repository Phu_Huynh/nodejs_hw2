const express = require('express');

const User = require('../../models/user');
const {generateJWTToken} = require('../../utils/jwt_helpers');

const auth = new express.Router();

auth.post('/register', checkRequestCredentialsBody, async (req, res) => {
  try {
    const {username, password} = req.body;

    const user = new User({username, password});
    await user.save();
    res.status(200).json({
      message: 'Successfully created a new user',
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

auth.post('/login', checkRequestCredentialsBody, async (req, res) => {
  try {
    const {username, password} = req.body;

    let user = {};
    try {
      user = await User.findByCredentials(username, password);
    } catch (err) {
      res.status(400).json({message: err.message});
      return;
    }

    const jwtToken = generateJWTToken({_id: user._id});
    res.status(200).json({message: 'Success', jwt_token: jwtToken});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

/**
 *
 * @param {Object} req
 * @param {Object} res
 * @param {funtion} next
 * @return {void}
 */
function checkRequestCredentialsBody(req, res, next) {
  const {username, password} = req.body;

  if (!username && !password) {
    res.status(400).json({
      message: 'username and password fields are missing!',
    });
    return;
  }
  if (!username) {
    res.status(400).json({
      message: 'username field is missing!',
    });
    return;
  }
  if (!password) {
    res.status(400).json({
      message: 'password field is missing!',
    });
    return;
  }
  next();
}

module.exports = auth;
