const express = require('express');
const apiRouter = require('./api/apiRouter');

const appRouter = new express.Router();

appRouter.use('/api', apiRouter);

module.exports = appRouter;
