const User = require('../models/user.js');
const {checkJWTToken} = require('../utils/jwt_helpers');

const tokenCheck = async (req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '');
    const decoded = checkJWTToken(token);
    const user = await User.findOne({_id: decoded._id});
    if (!user) {
      throw new Error('The user does not exist!');
    }

    req.token = token;
    req.user = user;
    next();
  } catch {
    res.status(400).send('Invalid jwt_token. Please login!');
  }
};

module.exports = tokenCheck;
