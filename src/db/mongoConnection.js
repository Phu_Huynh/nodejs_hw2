// establish connection with mongodb server

const mongoose = require('mongoose');
const connection =
  process.env.MONGODB_URL ||
  'mongodb://127.0.0.1:27017/notes-management-api';

mongoose.connect(connection).then(
    (/* value */) => {
      console.log('Connect to mongodb successfully!');
    },
    (err) => {
      console.error('Fail to connect to mongodb with error:', err);
    },
);
