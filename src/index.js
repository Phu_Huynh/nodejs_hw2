const app = require('./app.js');
const chalk = require('chalk');

const PORT = !!process.env.PORT ? process.env.PORT : 8080;

app.listen(PORT, () => {
  console.log(
      `\nExpress server has started at port ${chalk.bold.italic.yellow(PORT)}`,
  );
});
