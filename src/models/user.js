const mongoose = require('mongoose');
const {Schema} = mongoose;

const {hashString, checkHashString} = require('../utils/bcrypt_helpers');

const PASSWORD_MIN_LENGTH = !!parseInt(process.env.PASSWORD_MIN_LENGTH) ?
  parseInt(process.env.PASSWORD_MIN_LENGTH) :
  8;

const userSchema = new Schema(
    {
      username: {
        type: String,
        required: true,
        trim: true,
        unique: true,
      },
      password: {
        type: String,
        required: true,
        trim: true,
        minLength: PASSWORD_MIN_LENGTH,
        validate(value) {
          if (value.toLowerCase().includes('password') === false) {
            return true;
          } else {
            throw new Error('Password cannot contain "password"');
          }
        },
      },
    },
    {
    // Schema option to enable mongoose manage createAt and updateAt
      timestamps: true,
    },
);

// call user.populate(<<virtual field>>).execPopulate() to get the ref record
userSchema.virtual('notes', {
  ref: 'Note',
  localField: '_id',
  foreignField: 'userId',
});

// Hash the plain text password before saving to mongodb
userSchema.pre('save', async function(next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await hashString(user.password);
  }
  next();
});

// .methods is used to modify inherit methods
userSchema.methods.toJSON = function() {
  const user = this;
  const userObject = user.toObject();

  userObject.createdDate = userObject.createdAt;

  delete userObject.password;
  delete userObject.createdAt;
  delete userObject.updatedAt;
  delete userObject.__v;

  return userObject;
};

// .statics used to attach static method to Model created from this schema
userSchema.statics.findByCredentials = async function(username, password) {
  const user = await this.findOne({username});
  if (!user) {
    throw new Error('Unable to login: No such user!');
  }
  const isMatch = await checkHashString(password, user.password);
  if (!isMatch) {
    throw new Error('Unable to login: Invalid password');
  }
  return user;
};

const User = mongoose.model('User', userSchema);

module.exports = User;
