const mongoose = require('mongoose');
const {Schema, ObjectId} = mongoose;

const noteSchema = new Schema(
    {
      userId: {
        type: ObjectId,
        required: true,
        ref: 'User',
      },
      completed: {
        type: Boolean,
        default: false,
      },
      text: {
        type: String,
        required: true,
      },
    },

    {
    // Schema option to enable mongoose manage createAt and updateAt
      timestamps: true,
    },
);

// Modify .toJSON() method on Note model instance
noteSchema.methods.toJSON = function() {
  const note = this;
  const noteObject = note.toObject();

  noteObject.createdDate = noteObject.createdAt;

  delete noteObject.createdAt;
  delete noteObject.updatedAt;
  delete noteObject.__v;

  return noteObject;
};

const Note = mongoose.model('Note', noteSchema);

module.exports = Note;
