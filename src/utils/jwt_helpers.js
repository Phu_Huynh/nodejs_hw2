const jwt = require('jsonwebtoken');

const SECRET = !!process.env.JWT_SECRET ? process.env.JWT_SECRET : 'default';
const TOKEN_LIFE_TIME = !!process.env.JWT_TOKEN_LIFE ?
  process.env.JWT_TOKEN_LIFE :
  '30m';
/**
 *
 * @param {Object} payload
 * @return {string} jwt_token
 */
function generateJWTToken(payload) {
  const token = jwt.sign(payload, SECRET, {
    expiresIn: TOKEN_LIFE_TIME,
  });
  return token;
}

/**
 *
 * @param {string} jwtToken
 * @return {Object} decoded payload
 */
function checkJWTToken(jwtToken) {
  const decoded = jwt.verify(jwtToken, SECRET);
  return decoded;
}

module.exports = {
  generateJWTToken,
  checkJWTToken,
};
