require('dotenv').config();
require('./db/mongoConnection');
const express = require('express');
const morgan = require('morgan');

const path = require('path');
const fs = require('fs');

const appRouter = require('./routers/appRouter');

// Init server instance
const app = express();

// Parse json body
app.use(express.json());

// Create a write stream (in append mode)
const accessLogStream = fs.createWriteStream(
    path.join(...__dirname.split(/[\\/]/).slice(0, this.length - 2), '.log'),
    {
      flags: 'a',
    },
);

// Setup the logger with morgan; Apache style
app.use(morgan('combined', {stream: accessLogStream}));

// Routing
app.use('/', appRouter);

// Return error on any wrong path request
app.all('*', (req, res) => {
  const message = {
    message: 'Client error: Bad request',
  };
  res.status(400).json(message);
});

module.exports = app;
