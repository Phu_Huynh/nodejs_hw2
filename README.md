# Nodejs homework 2

## Set up

Create a .env file in **_package.json_**'s folder with the following information:

```
PORT=8080 // server running port
MONGODB_URL='mongodb://127.0.0.1:27017/notes-management-api'
PASSWORD_MIN_LENGTH=8 // user's password maxlength
SALT_ROUND= 10 // bcrypt hash round
JWT_SECRET='hellothere' // jwt secret
JWT_TOKEN_LIFE='15m' // jwt_token expiration time
```

find out [here](https://github.com/vercel/ms) for time format

## Usages

run the following command to start server

```console
whoami:~$ npm start
```

Incoming request will be logged in to **_.log_** file which locate in **_package.json_**'s folder
